var assert = require('assert');
var chai = require('chai');
//var expect = require('chai').expect;
//var should = require('chai').should();
var chaiHttp = require('chai-http');
var chaiAsPromised = require('chai-as-promised');
var app = require('../app');
//Configure chai
chai.use(chaiAsPromised).should();
chai.use(chaiHttp);



describe('Listing ships', () => {
    it('should resolve with a 200 status code', (done) => {
        const prom = new Promise((resolve) => {
            chai.request(app).get('/ships').end((err, res) => {
                resolve(res.status)
            })
        })
        prom.then((result) => {
            result.should.equal(200)
            done()
        })
    })
    it('should return an array', (done) => {
        const prom = new Promise((resolve) => {
            chai.request(app).get('/ships').end((err, res) => {
                resolve(res.body);
            })
        })
        prom.then((result) => {
            result.should.be.an('array')
            done()
        })

    })
})

describe.skip('Affecting sailors to a ship', () => {
    it('should work if parameters passed correctly', () => {

    })
})