var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../model/user');
var Ship = require('../model/ship');
var winston = require('winston');

/* GET ships listing. */
router.get('/', function(req, res) {
  winston.log('info','liste des bateaux');
	Ship.find(function(err,result){
		if (err) return winston.log('info',err);
		if (result.length == 0) return res.send('Pas de bateaux, pas de chocolat');
	})
	.populate('sailors')
	.exec(function(err,boats){
		if (err) return winston.log('info',err);
		let list = [];
		boats.forEach(function(el, i, array){
			winston.log('info','Bateau:'+el.name);
			list[i] = {name: el.name};
			list[i].sailors = [];
			el.sailors.forEach(function(sailor,){
				winston.log('info','Marin: %s', sailor.name);
				list[i].sailors.push(sailor.name);
			})
		})
		res.send(list);
	});
});

/* POST ship add. */
router.post('/',function(req,res){
	winston.log('info','ajouter un bateau : '+JSON.stringify(req.body));
	var newShip = new Ship(req.body);
	newShip.save(function(err,result){
		if (err) return winston.log('info',err);
		res.send('done :'+result);
	});
	
})

/* PUT ship upd. */
router.put('/',function(req,res){
	winston.log('info','modifier un bateau : '+JSON.stringify(req.body));
	Ship.findById(req.body._id,function(err,result){
		if (err) return winston.log('info',err);
		winston.log('info','bateau à modifier trouvé! '+result);
		result.name = req.body.name;
		result.class = req.body.class;
		result.state = req.body.state;
		result.capacity = req.body.capacity;
		result.save(function(err,updated){
			if (err) return winston.log('info',err);
			res.send('done :'+updated);
		})
	})
})

/* PUT ship sailor affectation */
router.put('/affect',function(req,res){
	if (req.body.sailors){
		winston.log('info','marins à affecter: '+req.body.sailors)
		//Search the boat
		Ship.find({name:req.body.ship},(err,result) =>{
			if (err) return winston.log('info',err);
			winston.log('info','bateau trouvé! '+result);
			result = result[0];
			//Sailors treatment
			req.body.sailors.forEach((x, index, array) => {
				//Search if the user already exists in db:
				User.find({name:x},(err,sailor) => {
					if (err) return winston.log('info',err);
					if (sailor.length > 0){
						winston.log('info','marin trouvé: '+(sailor));	
						result.sailors.push(sailor[0]._id);
						result.save(function(err,updated){
							if (err) return winston.log('info',err);
							winston.log('info','affectation au bateau '+result.name +' ok: '+sailor);
						});	
					} else {
						winston.log('info','création du marin : '+(sailor));	
						var newUser = new User({name: x});
						newUser.save((err,added)=>{
								if (err) return winston.log('info',err);
								winston.log('info','marin '+added+' créé');
								result.sailors.push( added._id);
								result.save(function(err,updated){
									if (err) return winston.log('info',err);
									winston.log('info','affectation au bateau '+result.name +' ok: '+added);
									
								});		
					  })
					}
				}).then(()=>{res.send('ok')})
			})
		})
	} else res.send('no sailor to affect!');
})	

/* DELETE ship */
router.delete('/',function(req,res){
	winston.log('info','supprimer un bateau : '+JSON.stringify(req.body));
	Ship.findById(req.body._id,function(err,result){
		if (err) return winston.log('info',err);
		winston.log('info','bateau à supprimer trouvé!');
		result.remove(function(err,x){
			if (err) return winston.log('info',err);
			res.send('done :'+x);
		});
	})
})
module.exports = router;