var express = require('express');
var router = express.Router();
var winston = require('winston');

var mongoose = require('mongoose');
var User = require('../model/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  winston.log('info','lister les users');
	User.find(function(err,users){
		if (err) return winston.log('info',err);
		if (users.length == 0) return res.send('no users!');
		res.send(users);
	})
});

/* POST user add. */
router.post('/',function(req,res){
	winston.log('info','ajouter un user : '+JSON.stringify(req.body));
	
	var newuser = new User(req.body);
	newuser.save(function(err,result){
		if (err) return winston.log('info',err);
		res.send('done :'+result);
	});
	
})

/* PUT user upd. */
router.put('/',function(req,res){
	winston.log('info','modifier un user : '+JSON.stringify(req.body));
	
	user.findById(req.body._id,function(err,result){
		if (err) return winston.log('info',err);
		winston.log('info','user à modifier trouvé!');
		result.name = req.body.name;
		result.age = req.body.age;
		result.function = req.body.function;
		winston.log('info',result);
		result.save(function(err,updated){
			if (err) return winston.log('info',err);
			res.send('done :'+updated);
		})
	})
})

/* DELETE user */
router.delete('/',function(req,res){
	winston.log('info','supprimer un user : '+JSON.stringify(req.body._id));
	User.findById(req.body._id,function(err,result){
		if (err) return winston.log('info',err);
		winston.log('info','user à supprimer trouvé! ',result);
		result.remove(function(err,x){
			if (err) return winston.log('info',err);
			res.send('done :'+x);
		});
	})
})

module.exports = router;
