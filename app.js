var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var winston = require('winston');
winston.add(winston.transports.File, { filename: 'test.log' });

var index = require('./routes/index');
var users = require('./routes/users');
var ships = require('./routes/ships');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/ships', ships);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var mongoose = require('mongoose'); //réquisition de la ressource mongoose
mongoose.connect('mongodb://localhost/pj_mongoose',{ useNewUrlParser: true}); //connexion à la base

var db = mongoose.connection; //notre obj connection
db.on('error', winston.error.bind(winston, 'connection error:')); //gestion d'erreurs
db.once('open', function() { //once désigne un callback
  winston.log('info',"we\'re connected!"); //on affiche dans la console...
	//mongoose.set('debug', true);
});

module.exports = app;
