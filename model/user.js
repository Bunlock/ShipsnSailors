var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
	name: {type: String, required:true},
	age: Number,
	function: {type: String, enum:["Captain","Sailor","Lieutenant"]}
});
var User = mongoose.model('user', userSchema);

module.exports = User;