const mongoose = require('mongoose');
var shipSchema = mongoose.Schema({
	name: {type: String, unique:true},
	class:{
		type:String, 
		enum:['Corvet','Cruiser','Dreadnought','Cruiseship','Cargo'],
		required: [true, 'A ship ALWAYS has a class!']
	},
	state:{type:String, enum:['planned','built','sailing','sunk','drydocked']},
	builddate: {
		type: Date
		//,required: function(){return [this.state != 'planned','need a build date Dude...'];}
	},
	sailors:[{type: mongoose.Schema.Types.ObjectId, ref:'user'}],
	capacity:{type: Number, min: 0, max: 2000},
	img: String
}); 

var Ship = mongoose.model('ship', shipSchema);
	
module.exports = Ship;